## Purpose

This repo contains:
- some shell scripts to set up a local Kubernetes cluster (based on kubeadm, k3s or k0s)
- some yaml manifests to install sample applications for test purposes (Elastic stack, Voting application, ...)

## Quickly create local Kubernetes clusters

There are only 2 prerequisites you need to install before running one of the cluster creation script:

- [kubectl](https://kubernetes.io/docs/tasks/tools/#kubectl) to communicate with the cluster from your host machine
- [Multipass](https://multipass.run), a tool that allows you to spin up Ubuntu VMs in a matter of seconds on a Mac, Linux, or Windows workstation. Depending upon your OS, Multipass uses Hyper-V, HyperKit, KVM, or VirtualBox natively for the fastest startup time. 

### Kubeadm cluster

![Kubeadm](./images/logo_kubeadm.png)

The first script, available on https://luc.run/k8s.sh, allows you to create a kubeadm cluster. It can be used with the default options as follow:

```
curl https://luc.run/k8s.sh | bash -s
```

It performs the following actions:

- creation of 2 Ubuntu VMs using Multipass
- installation of the required packages on each VM (kubeadm, containerd, …)
- cluster initialization on the control-plane VM
- join a worker node

It takes a couple of minutes to create the cluster depending upon the number of worker nodes (the script could probably be optimized a bit so that some actions run in parallel).

Once the creation is done you just need to configure your local kubectl with the command provided in the output:

```
export KUBECONFIG=$PWD/kubeconfig.cfg
```

Then you can use this newly created cluster and start by listing the nodes (it can take a few tens of seconds for the nodes to reach the Ready status):

```
$ kubectl get no
NAME            STATUS   ROLES           AGE   VERSION
control-plane   Ready    control-plane   51s   v1.25.2
worker1         Ready    <none>          31s   v1.25.2
```

You can download this script and use it locally if you prefer. The available options are the following ones:

```
$ ./k8s.sh
...
-v VERSION: Kubernetes version (default is 1.25.2)
-w WORKERS: number of worker nodes (defaults is 1)
-c CPU: cpu used by each Multipass VM (default is 2)
-m MEM: memory used by each Multipass VM (default is 2G)
-d DISK: disk size (default 10G)
-p CNI: Network plugin among weavenet, calico and cilium (default)
-D: destroy the cluster
```

Once you’re done using the cluster you can destroy it as follows:

```
curl https://luc.run/k8s.sh | bash -s -- -D
```

or if you have retrieved the script locally

```
./k8s.sh -D
```

### k3s cluster

![k3s](./images/logo_k3s.png)

The second script, available on https://luc.run/k3s.sh, allows to create a k3s cluster (k3s is a lightweight Kubernetes distribution created by Rancher / SUSE). It can be used with the default options as follow:

```
curl https://luc.run/k3s.sh | bash -s
```

It performs the following actions:

- creation of 2 Ubuntu VMs using Multipass
- cluster initialization on the control-plane VM
- join a worker node

It takes a couple of minutes to create the cluster depending upon the number of worker nodes. It’s much quicker than the creation of a kubeadm cluster as k3s is more lightweight.

Once the creation is done you just need to configure your local kubectl with the command provided in the output

```
export KUBECONFIG=$PWD/kubeconfig.k3s
```

Then you can use this newly created cluster and start by listing the nodes (it can take a few tens of seconds for the nodes to reach the Ready status):

```
$ kubectl get no
NAME            STATUS   ROLES                  AGE   VERSION
control-plane   Ready    control-plane,master   70s   v1.24.6+k3s1
worker1         Ready    <none>                 61s   v1.24.6+k3s1
```

You can download this script and use it locally if you prefer. The available options are the following ones:

```
$ ./k3s.sh -h
...
-w WORKERS: number of worker nodes (defaults is 1)
-c CPU: cpu used by each VM (default is 2)
-m MEMORY: memory used by each VM (default is 2G)
-d DISK: disk space used by each VM (default is 10G)
-D: destroy the cluster
```

Once you’re done using the cluster you can destroy it as follows:

```
curl https://luc.run/k3s.sh | bash -s -- -D
```

or if you have retrieved the script locally

```
./k3s.sh -D
```

### K0s cluster

![k0s](./images/logo_k0s.png)

The third script, available on https://luc.run/k0s.sh, allows to create a k0s cluster (k0s is a Kubernetes distribution created by Mirantis). It can be used with the default options as follow:

```
curl https://luc.run/k0s.sh | bash -s
```

It performs the following actions:

- creation of 3 Ubuntu VMs using Multipass
- cluster initialization on the control-plane VM
- join 2 worker nodes

It takes a couple of minutes to create the cluster depending upon the number of worker nodes. As for k3s, it’s much quicker than the creation of a kubeadm cluster.

Once the creation is done you just need to configure your local kubectl with the command provided in the output

```
export KUBECONFIG=$PWD/kubeconfig.k0s
```

Then you can use this newly created cluster and start by listing the nodes (it can take a few tens of seconds for the nodes to reach the Ready status):

```
$ kubectl get no
NAME      STATUS   ROLES    AGE   VERSION
worker1   Ready    <none>   27s   v1.25.2+k0s
worker2   Ready    <none>   32s   v1.25.2+k0s
```

Note: the control-plane node is not listed, only worker nodes are.

You can download this script and use it locally if you prefer. The available options are the following ones:

```
$ ./k0s.sh -h
...
-v VERSION: Kubernetes version (default is 1.25.2)
-w WORKERS: number of worker nodes (defaults is 2)
-c CPU: cpu used by each Multipass VM (default is 2)
-m MEM: memory used by each Multipass VM (default is 2G)
-d DISK: disk size (default 10G)
-D: destroy the cluster
```

Once you’re done using the cluster you can destroy it as follows:

```
curl https://luc.run/k0s.sh | bash -s -- -D
```

or if you have retrieved the script locally

```
./k0s.sh -D
```

## Quickly install sample applications

This repo contains some yaml manifests to install sample applications for test purposes

### Guestbook

Basic application to enter and display comments:

```
kubectl apply -f https://luc.run/guestbook.yaml
```

The application is launched in the namespace *guestbook* and the frontend exposed through a NodePort Service on port *31000*.

![Guestbook](./images/guestbook.png)

### Elastic

Well known stack used for log managements. It is composed of:
- Logstash: log ingestion
- Elasticsearch: log indexing
- Kibana: log visualization

![Elastic](./images/elastic.jpeg)

```
kubectl apply -f https://luc.run/elastic.yaml
```

All the components are created in the namespace *elastic* and are exposed through NodePort Services.

| Component  | NodePort |
| ---------  | -------- |
| Logstash   | 31500    |
| Kibana     | 31501    |

If an Ingress controller is running:
- Logstach is available from logstash.elk.io
- Kibana is available from kibana.elk.io

:fire: domain names need to be resolved with the Ingress Controller external IP first

### TICK stack
 
Stack mainly used for monitoring purposes. It is composed of:
- Telegraf: data ingestion
- InfluxDB: data storage (time series)
- Chronograf: data visualization
- Kapacitor: real time data analysis

![TICK](./images/tick.png)

```
$ kubectl apply -f https://luc.run/tick.yaml
```

All the components are created in the namespace *tick* and are exposed through NodePort Services.

| Component  | NodePort |
| ---------  | -------- |
| Telegraf   | 31601    |
| Chronograf | 31602    |
| Kapacitor  | 31603    |

If an Ingress controller is running:
- Telegraf is available from *telegraf.tick.com*
- Chronograf is available from *chronograf.tick.com*

:fire: domain names need to be resolved with the Ingress Controller external IP first

### Voting App

Famous application from Docker, it allows to vote and visualize the results:

![Voting App](./images/votingapp.png)

```
kubectl apply -f https://luc.run/vote.yaml
```

All the Voting App components are created in the namespace *vote*. Frontend conponents are exposed through NodePort Services.

| Component  | NodePort |
| ---------  | -------- |
| Vote interfate   | 31001 |
| Result interface | 31002 |

![Vote](./images/vote.png) ![Result](./images/result.png)

If an Ingress controller is running:
- the vote interface can be accessed on vote.votingapp.com
- the result interface can be accessed on result.votingapp.com

:fire: domain names need to be resolved with the Ingress Controller external IP first

### Ghost

Blogging application

```
kubectl apply -f https://luc.run/ghost.yaml
```

The web application is exposed through a NodePort Service on port 31005

![Ghost](./images/ghost.png)
