<#
.SYNOPSIS
Helper script that created a Kubernetes cluster using Multipass and Kubeadm

.DESCRIPTION
This script creates a single master / multiple workers K8s cluster using kubeadm
usage: k8s.ps1 [options]
-v Version: version of the kubernetes cluster to setup (default is 1.20.4)
-w Workers: number of worker nodes (defaults is 1)
-c Cpu: cpu used by each Multipass VM (default is 1)
-m Mem: memory used by each Multipass VM (default is 2G)
-d Disk: disk size (default 5G)
-n Name: prefix of node name (default is k8s)

Prerequisites:
- Multipass (https://multipass.run) must be installed
- kubectl must be installed

.EXAMPLE
./k8s.ps1

.LINK
https://luc.run/
#>

param(
  [string]
  $Name="k8s",

  [int]
  $Workers=1,

  [int]
  $Cpu=1,

  [string]
  $Mem="2G",

  [string]
  $Disk="5G",

  [string]
  $Version="1.20.4-00"
)

# Define number of nodes
$Nodes=$($Workers + 1)

function Usage {
  param(
    [string]
    $Message=""
  )
  Write-Host "---`n$Message`n---"
  Write-Host 
  Write-Host "This script creates a single master / multiple workers K8s cluster using *kubeadm*"
  Write-Host "usage: k8s.sh [options]"
  Write-Host "-v Version: version of the kubernetes cluster to setup (default is 1.20.4)"
  Write-Host "-w Workers: number of worker nodes (defaults is 1)"
  Write-Host "-c Cpu: cpu used by each Multipass VM (default is 1)"
  Write-Host "-m Mem: memory used by each Multipass VM (default is 2G)"
  Write-Host "-d Disk: disk size (default 5G)"
  Write-Host "-n Name: prefix of node name (default is k8s)"
  Write-Host
  Write-Host "Prerequisites:"
  Write-Host "- Multipass (https://multipass.run) must be installed"
  Write-Host "- kubectl must be installed"
  exit 0
}

# Make sure Multipass is there
function Check-Multipass {
  if ((Get-Command "multipass.exe" -ErrorAction SilentlyContinue) -eq $null)
  {
     Usage -Message "Please make sure multipass.exe (https://multipass.run) is installed and available in your PATH"
  }
}

# Make sure kubectl is there
function Check-Kubectl {
  if ((Get-Command "kubectl.exe" -ErrorAction SilentlyContinue) -eq $null)
  {
     Usage -Message "Please make sure kubectl.exe is installed and available in your PATH"
  }
}

# Create the master and workers VM using Multipass
function Create-Vms {
  for($i = 1; $i -le $Nodes; $i++){
    Write-Host "-> creating VM [$Name-$i]"
    Try{
      multipass launch --name $Name-$i --mem $Mem --cpus $Cpu --disk $Disk
      Write-Host "VM $Name-$i created"
    }
    Catch{
      Usage -Message "Error creating [$Name-$i] VM"
    }
  }
}

# Install dependencies on each node
function Install-Dependencies {
  for($i = 1; $i -le $Nodes; $i++){
    Write-Host "-> installing dependencies on node [$Name-$i]"
    Try{
      multipass exec $Name-$i -- /bin/bash -c "echo 1 | sudo tee /proc/sys/net/ipv4/ip_forward";
      multipass exec $Name-$i -- sudo modprobe overlay;
      multipass exec $Name-$i -- sudo modprobe br_netfilter;
      multipass exec $Name-$i -- sudo apt-get install containerd -y;
      multipass exec $Name-$i -- sudo mkdir -p /etc/containerd;
      multipass exec $Name-$i -- sudo containerd config default /etc/containerd/config.toml;
      # Installing Kubernetes components
      multipass exec $Name-$i -- /bin/bash -c "curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -";
      multipass exec $Name-$i -- sudo apt-add-repository "deb http://apt.kubernetes.io/ kubernetes-xenial main";
      multipass exec $Name-$i -- sudo apt-get update;
      multipass exec $Name-$i -- sudo apt-get install -y kubelet=$Version kubeadm=$Version kubectl=$Version;
    }
    Catch{
      Usage -Message "Error installing dependencies on node [$Name-$i]"
    }
  }
}

function Init-Cluster {
  Write-Host "-> initializing cluster on [$Name-1]"
  Try{
    multipass exec $Name-1 -- sudo kubeadm init --ignore-preflight-errors=NumCpu,Mem
  }
  Catch{
    Usage -Message "Error during cluster init"
  }
  Write-Host "cluster initialized"
}

function Get-Config {
  Write-Host "-> getting kubeconfig file"
  Try{
   (multipass exec $Name-1 -- sudo cat /etc/kubernetes/admin.conf) > kubeconfig
  }
  Catch{
    Usage -Message "Error retreiving kubeconfig"
  }
}

# Add worker nodes
function Add-Nodes {
  $JOIN=$(multipass exec $Name-1 -- /bin/bash -c "sudo kubeadm token create --print-join-command 2>/dev/null | tr -d '\r'")

  if (${Workers} -ge 1) {
    for($i = 1; $i -le $Workers; $i++){
      Write-Host "-> adding worker nodes [$Name-$($i+1)]"
      Try{
        multipass exec $Name-$($i+1) -- /bin/bash -c "sudo $JOIN"
      }
      Catch{
        Usage -Message "Error while joining worker node [$Name-$($i+1)]"
      }
    }
  } else {
    Write-Host "-> no worker will be added"
  } 
}

function Install-Network-Plugin {
  Write-Host "-> installing WeaveNet network plugin"
  $VERS=$(multipass exec $Name-1 -- /bin/bash -c "sudo kubectl --kubeconfig=/etc/kubernetes/admin.conf version | base64 | tr -d '\n'")
  Try{
    multipass exec $Name-1 -- sudo kubectl apply --kubeconfig=/etc/kubernetes/admin.conf -f "https://cloud.weave.works/k8s/net?k8s-version=$VERS"
  }
  Catch{
    Usage -Message "Error while installing network plugin"
  }
}

function Remove-Taint {
  Write-Host "-> Remove taint from the master node"
  multipass exec $Name-1 -- sudo kubectl --kubeconfig=/etc/kubernetes/admin.conf taint nodes --all node-role.kubernetes.io/master-
}

function Next {
  Write-Host "Cluster is up and ready !"
  Write-Host "Please follow the next steps:"
  Write-Host
  Write-Host "- Configure your local kubectl:"
  Write-Host "`$Env:KUBECONFIG=`"`$pwd\kubeconfig`""
  Write-Host
  Write-Host "- Check the nodes getting ready (might take around 1 minute):"
  Write-Host "kubectl get nodes"
  Write-Host
}

Check-Multipass
Check-Kubectl
Create-Vms
Install-Dependencies
Init-Cluster
Get-Config
Add-Nodes
Install-Network-Plugin
Remove-Taint
Next
