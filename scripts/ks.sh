#!/usr/bin/env bash

function usage {
  echo $1
  exit 1
}

# Make sure Multipass is there
function check-multipass {
  echo "-> Making sure Multipass (https://multipass.run) is installed"
  multipass version 1>/dev/null 2>&1 || usage "Please install Multipass (https://multipass.run)"
}

# Make sure kubesphere VM does not already exist
function check-existing-vm {
  echo "-> Checking existing multipass VMs"
  if [ "$(multipass list | grep kubesphere)" != "" ]; then
    usage "Kubesphere VM already exists"
  fi
}

# Define cloud-init file
function cloud-init {
cat<<EOF > ./cloud-init-ks.yaml
packages:
  - conntrack
  - socat
  - ebtables
  - ipset

runcmd:
  # Install k3s
  - curl -sSLf https://get.k3s.io | INSTALL_K3S_VERSION="v1.22.2+k3s2" INSTALL_K3S_EXEC="server --no-deploy traefik" sh

  # Make Kubeconfig available to "ubuntu" user
  - mkdir /home/ubuntu/.kube
  - cp /etc/rancher/k3s/k3s.yaml /home/ubuntu/.kube/config
  - chown -R ubuntu:ubuntu  /home/ubuntu/.kube/ /etc/rancher/k3s/k3s.yaml

  # Install Kubespher via Helm
  - kubectl apply -f https://github.com/kubesphere/ks-installer/releases/download/v3.2.0/kubesphere-installer.yaml
  - kubectl apply -f https://github.com/kubesphere/ks-installer/releases/download/v3.2.0/cluster-configuration.yaml
EOF
}

function create-environment {
  echo "-> Creating ubuntu VM with k3s and kubesphere (this will take a couple of minutes)"
  cat ./cloud-init-ks.yaml | multipass launch -n kubesphere -d 20G -m 4G -c 3 --cloud-init - 18.04 || usage "error while creating VM"
  rm ./cloud-init-ks.yaml
}

function get-kubeconfig {
  echo "-> Retrieve kubeconfig and save it locally"

  # Get generated kubeconfig file and change the URL so it uses the VM's IP instead of localhost
  multipass exec kubesphere cat /etc/rancher/k3s/k3s.yaml > ./kubesphere.kubeconfig.tmp
  IP=$(multipass info kubesphere | grep IP | awk '{print $2}')
  sed "s/127.0.0.1/$IP/" ./kubesphere.kubeconfig.tmp > ./kubesphere.kubeconfig
  rm ./kubesphere.kubeconfig.tmp
}

function get-info {
  echo "-> Waiting for Kubesphere api to be available (please be patient :) )"
  multipass exec kubesphere -- /bin/bash -c 'until [[ $(kubectl -n kubesphere-system get po -l app=ks-apiserver 2>1 | grep Running) ]]; do sleep 20; done'

  # Get UI info
  IP=$(multipass info kubesphere | grep IPv4 | awk '{print $2}')
  PORT=$(multipass exec kubesphere -- /bin/bash -c 'kubectl -n kubesphere-system get svc/ks-console -o jsonpath="{.spec.ports[0].nodePort}"')

  echo
  echo "Kubesphere UI is available on http://$IP:$PORT"
  echo
  echo "1. You can access kubesphere from your local machine exporting the KUBECONFIG env variable:"
  echo "export KUBECONFIG=$PWD/kubesphere.kubeconfig"
  echo
  echo "2. Use the default credentials: admin / P@88w0rd"
  echo
  echo "3. If needed, You can run a shell in the newly created kubesphere VM:"
  echo "multipass shell kubesphere"
  echo
}

check-multipass
check-existing-vm
cloud-init
create-environment
get-kubeconfig
get-info
